/*--------------------------------------------------------------------
Name: Tylar Hanson, Lucas Mireles
Date: 18 Oct 16
Course: ECE 382
File: test5.c
Event: Lab 5

Purp: Measure IR Pulses

Doc: None.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/

#include <msp430.h>

typedef		unsigned  char		int8;
typedef		unsigned short		int16;

#define SAMPLE_SIZE			48
#define	IR_DECODER_PIN		(P2IN & BIT6)
void initMSP430();

// -----------------------------------------------------------------------
// -----------------------------------------------------------------------
void main(void) {

	int16 time1[SAMPLE_SIZE], time0[SAMPLE_SIZE];
	int8  i;


	initMSP430();				// Set up MSP to process IR and buttons

    while (1)  {

		while(IR_DECODER_PIN != 0);			// IR input is nominally logic 1

		for(i=0; i<SAMPLE_SIZE; i++) {
			//**Note** If you don't specify TA1 or TA0 then TAR defaults to TA0R
			TAR = 0;						// reset timer and
			while(IR_DECODER_PIN==0);		// wait while IR is logic 0
			time0[i] = TAR;					// and store timer A

			TAR = 0;						// reset timer and
			while(IR_DECODER_PIN != 0);		// wait while IR is logic 1
			time1[i] = TAR;					// and store timer A

		} // end for
    } // end while
} // end main


// -----------------------------------------------------------------------
// -----------------------------------------------------------------------
void initMSP430(){

	IFG1=0; 					// clear interrupt flag1
	WDTCTL=WDTPW+WDTHOLD; 		// stop WD

	BCSCTL1 = CALBC1_8MHZ;
	DCOCTL = CALDCO_8MHZ;
										// Fill in the next six lines of code.
	// (8e6 clks/ 1 sec) * (1 cnt/ 8 clks) * (65e-3 sec)  = 65,000
	TA0CCR0 = 65000;					// ensure TAR is clear
	TA0CTL &= ~TAIFG;					// create a ~65 ms roll-over period with TA0CCR0
	TA0CTL |= ID_3|TASSEL_2|MC_1;		// Use 1:8 prescalar off SMCLK

	P2DIR &= ~BIT6;						// Set up P2.6 as GPIO not XIN
	P2SEL &= ~BIT6;						// This action takes
	P2SEL2 &= ~BIT6;					// three lines of code.

}
