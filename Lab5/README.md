# Lab 5 - Interrupts - "Remote Control Decoding"

## By C2C Tylar Hanson and C2C Lucas Mireles

## Table of Contents
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Preliminary Design](#preliminary-design)
3. [Software flow chart or algorithms](#software-flow-chart-or-algorithms)
4. [Hardware schematic](#hardware-schematic)
5. [Debugging](#debugging)
6. [Testing methodology or results](#testing-methodology-or-results)
7. [Answers to Lab Questions](#answers-to-lab-questions)
8. [Observations and Conclusions](#observations-and-conclusions)
9. [Documentation](#documentation)
 
### Objectives or Purpose 
- Use our knowledge of interrupts and the Timer_A subsytem to reverse engineer a remote control.
- Required Functionality
	- Turn an LED on and off with one button on the remote. 
	- Turn another LED on and off with a different button. 
	- Turn both LEDs on and off with a third button.
- A Functionality
	- Use the buttons on a remote control to either control the etch-a-sketch game or the pong game in lab 4.

### Preliminary design
Below describes which line of the for loop in test.c is executed at which part of the pulse.

![](images/irWave.gif)

Additionally, below is the signal received when the up button is pressed as shown on the logic analyzer.

![](images/signal_time.jpg)

When comparing the timer A counts with the duration of the pulse, we can look at the Start logic 1 half pulse and compare the 4.5 ms with the 4433 counts and see that they are very similar.

##### Table 1: Pulse Characteristics
This table shows the average count for each type of bit. It also shows the standard deviation for the logic bits. This is used in `start5.h` to define constants needed to classify pulses. 

![](images/PulseCharacteristicTable.JPG)

##### Table 2: Up Button Waveform
This shows the values captured in the time0 and time1 arrays. Between the start and stop bits, if the count was greater than 1500 the bit was classified as a data logic 1 and 0 if other wise. Using this same method we classified the hex codes for ten button, shown in Table 3.

![](images/UpButton.JPG)

##### Table 3: Hex Codes
Using the same method described above table two, we found the hex codes for nine other buttons.

![](images/HexCodes.JPG)


### Software flow chart or algorithms
This is a flowchart of pseudo code for main and the port two interrupt service routine.
![](images/Lab5.png)

### Hardware schematic
This image shows how the infrared sensor was connected to our MSP430.

![](images/HardwareSchematic.JPG)

For this lab we used a VISIO remote.

![](images/TVRemote.jpg)

### Debugging
One particular issue we had was trying to implement A Functionality. Initially we wanted to pick which functionality to do when the board powered on. However, the LCD needs to be power cycled after being programmed so we changed it to do one functionality or the other at a time. 

### Testing methodology or results

##### Required Functionality
Our required functionality was to toggle an LED  with one button on the remote, toggle a different LED with a different button, and to toggle both LEDs with a third button. To test this, we used the left button on the remote to toggle the left red LED, the right button to toggle the right green LED, and we chose to "OK" button  to toggle both the lights. Below is the link to the video that demonstrates the required functionality.
[Required Functionality](https://www.youtube.com/watch?v=qA5Op_tB0fU)

##### A Functionality
A Functionality is implementing our remote with our etch-e-sketch from Lab 4. We used the buttons up, down, left, right, and okay to implement the game using the remote. The okay button simply toggled the color of the square and the direction buttons controlled the direction of the drawing just as they did in Lab 4. Below is the the link to the video that demonstrates A Functionality.
[A Functionality](https://www.youtube.com/watch?v=S50TjiER_0o)

### Answers to Lab Questions
- How long will it take the timer to rollover?
	- 65 ms
- How long does each timer count last?
	- 1 us

### Observations and Conclusions
The objective of this lab was to use interrupts and an infrared sensor to receive and decode an IR pulse. To achieve required functionality we had to toggle some LEDs with button presses on a TV remote. For A Functionality we used the same remote to control the etch-a-sketch program created in lab 4. We got each functionality to work. In this lab we learned how to use different types of interrupts in order to simplify complex code. This will be helpful when we start to work with the robot next lab.

### Documentation
None.