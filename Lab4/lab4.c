/*--------------------------------------------------------------------
Name: Tylar Hanson, Lucas Mireles
Date: 18 Oct 16
Course: ECE 382
File: pong1.c
Event: Lab 4

Purp: Includes functions to create a ball, move the ball, and detect
collisions with the screen edges.

Doc: None.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/

#include "lab4.h"
#include "fonts.h"

Ball createBall( int xPos, int yPos, int xVel, int yVel, int radius ){
	volatile Ball result;
	result.pos.x = xPos;
	result.pos.y = yPos;
	result.vel.x = xVel;
	result.vel.y = yVel;
	result.radius = radius;
	return result;
}

Ball moveBall( Ball ballToMove ){
	volatile Ball result;
	result.pos.x = ballToMove.pos.x + ballToMove.vel.x;
	result.pos.y = ballToMove.pos.y + ballToMove.vel.y;
	result.vel.x = ballToMove.vel.x;
	result.vel.y = ballToMove.vel.y;
	result.radius = ballToMove.radius;

	if( topCollision(result) ){
		result.vel.y *= -1;
		}
	if( xCollision(result) ){
		result.vel.x *= -1;
	}
	return result;
}

char xCollision( Ball checkMe ){
	char result = FALSE;
	if( (checkMe.pos.x >= SCREEN_WIDTH - checkMe.radius) | (checkMe.pos.x <= 0) ){ result = TRUE; }
	return result;
}

char topCollision( Ball checkMe ){
	char result = FALSE;
	if(checkMe.pos.y <= 0){ result = TRUE; }
	return result;
}

char bottomCollision( Ball checkMe ){
	char result = FALSE;
	if(checkMe.pos.y >= SCREEN_HEIGHT - checkMe.radius){ result = TRUE; }
	return result;
}

char paddleCollision( Ball checkMe, int paddleX, int paddleY ){
	char result = FALSE;
	if(checkMe.pos.x >= paddleX - 5 && checkMe.pos.x <= paddleX + PADDLE_LENGTH - 5 ){ // If ball is within the left and right edge of the paddle
		if(checkMe.pos.y >= paddleY - checkMe.radius - PADDLE_HEIGHT){ // And if ball is on or lower than the paddle
			result = TRUE;
		}
	}
	return result;
	}
