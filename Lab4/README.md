# Lab 4 - "Etch-a-sketch and Pong"

## By C2C Tylar Hanson and C2C Lucas Mireles

## Table of Contents
1. [Objectives or Purpose](#objectives-or-purpose)
2. [PreLab](#prelab)
3. [Debugging](#debugging)
4. [Testing methodology or results](#testing-methodology-or-results)
5. [Answers to Lab Questions](#answers-to-lab-questions)
6. [Observations and Conclusions](#observations-and-conclusions)
7. [Documentation](#documentation)
 
### Objectives or Purpose 
- In this lab, We'll use C to create an etch-a-sketch-type program that utilizes some subroutines from Lab 3.
- Modify your assembly drawBox function to take in 3 values: an x coordinate, a y coordinate, and a color.
- Create an etch-a-sketch program using the directional buttons of the LCD boosterpack to control the position of the paint brush. The paint brush will draw 10x10 blocks of pixels. The user will change the position of the paint brush by pressing the directional buttons. Each button press will move the cursor 10 pixels in the direction pressed. Pressing the auxiliary button (S1) will toggle the mode of the paint brush between filling squares and clearing squares.

### PreLab

##### Pseudocode Flowchart
![](images/Lab4.png)
##### Figure 1: Data Types                                                     
![](images/DataTypes.JPG)
##### Figure 2: Typedefs
![](images/Typedefs.JPG)
##### Figure 3: Calling/Return Convention                                                                                    
![](images/CallingReturnConventions.JPG)
##### Figure 4: Disassembled View
![](images/DisassembledView.JPG)

##### Cross language build constructs
- What is the role of the `extern` directive in a .c file?
	- When using extern with variables the variable is only declared, and not defined. And when trying to use that variable from within a function will give you a compiler error. For both variables and functions, extern makes the function or variable usable from anywhere within the program as long as they are defined. So they are like global variables and functions. 
- What is the role of the `.global` directive in an .asm file?
	- It makes a label externally available to the linker. So functions made in an assembly file can be called directly from a C file.

### Debugging
When testing the required functionality the block of paint would got past the top and left edges of the screen, but behaved as expected for the bottom and right edges. Through our troubleshooting we realized that we were using unsigned integers to keep track of the paint blocks location. By doing this the number could not be negative and the way we know if the block is off the top or left edge is if the x or y value is negative. For B functionality the ball did not move as expected at first. We looked at our collision functions and rearranged some lines of code in a more logical manner, then the ball moved across the screen as we wanted it to.

### Testing methodology or results
##### Required Functionality
To test our etch-a-sketch program we expect the paint block to move by ten pixels in the direction of the button pushed while staying within the bounds of the screen. The auxiliary  button should toggle the color of the block between blue and black. The following video shows the functionality of this program.
[Required Functionality](https://www.youtube.com/watch?v=DTgjQl0U2eY)
##### B Functionality
For B Functionality a ball should move across the screen and bounce off all four walls. The following video shows the functionality of this program.
[B Functionality](https://www.youtube.com/watch?v=VNgoeJpmw7Y)
##### A Functionality
This part takes B Functionality one step further by adding a paddle that moves left or right at the bottom of the screen. The ball should bounce off this paddle as well as the top, left, and right wall. If the ball hits the bottom edge of the screen the game is over and the program stops. The following video shows the functionality of this program.
[A Functionality](https://www.youtube.com/watch?v=7E40BtWZgRU)

### Observations and Conclusions
For required functionality the goal was to create an etch-a-sketch program while keeping the paint block within the bounds of the screen. For B Functionality we needed to create a block that moved across the screen while bouncing off each edge of the screen. A Functionality took it a step further and added a paddle to make a pong game that ended when the ball made it past the paddle. We completed every functionality. If we were to write this program again we would consider creating a new structure to represent the paddle for A functionality. 

### Documentation
None.