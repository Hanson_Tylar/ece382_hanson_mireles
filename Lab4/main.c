#include <msp430g2553.h>
#include "lab4.h"

extern void initMSP();
extern void initLCD();
extern void Delay160ms();
extern void Delay40ms();
extern void clearScreen();
extern void drawBox(unsigned int x, unsigned int y, unsigned int color );
extern void drawPaddle(unsigned int x, unsigned int y, unsigned int color );

#define		TRUE			1
#define		FALSE			0
#define		UP_BUTTON		(P2IN & BIT2)
#define		DOWN_BUTTON		(P2IN & BIT1)
#define		LEFT_BUTTON		(P2IN & BIT0)
#define		RIGHT_BUTTON	(P2IN & BIT3)
#define		AUX_BUTTON		(P1IN & BIT3)

void requiredFunctionality(){
	unsigned int COLOR = 0x4416;
	int x, y;
	x=5;
	y=5;
	drawBox(x, y, COLOR);

	while(1) {
		Delay160ms();
		if(UP_BUTTON == 0){
			y -= 10;
			if(y<=5){y=5;}
			drawBox(x, y, COLOR);
		}
		else if(DOWN_BUTTON == 0){
			y += 10;
			if(y>=SCREEN_HEIGHT-15){y=SCREEN_HEIGHT-15;}
			drawBox(x, y, COLOR);
		}
		else if(RIGHT_BUTTON == 0){
			x+= 10;
			if(x>=SCREEN_WIDTH-15){x=SCREEN_WIDTH-15;}
			drawBox(x, y, COLOR);
		}
		else if(LEFT_BUTTON == 0){
			x-= 10;
			if(x<=5){x=5;}
			drawBox(x, y, COLOR);
		}
		else if(AUX_BUTTON == 0){
			if(COLOR == 0){
				COLOR = 0x4416;
			}
			else{
				COLOR = 0;
			}
			drawBox(x, y, COLOR);
		}
	}
}

BFunctionality(){
	unsigned int COLOR = 0x4416;
	Ball myBall = createBall( XPOS_START, YPOS_START, XVEL_START, YVEL_START, RADIUS );
	while (1){
		drawBox(myBall.pos.x, myBall.pos.y, COLOR);
		Delay40ms();
		Delay40ms();
		drawBox(myBall.pos.x, myBall.pos.y, 0);
		myBall = moveBall (myBall);
		if(bottomCollision(myBall)){
			myBall.vel.y *= -1;
		}
	}
}

AFunctionality(){
	unsigned int COLOR = 0x4416;
	int GAMEOVER = FALSE;
	int score = 0;
	int paddleX = SCREEN_WIDTH/2 - 15;
	int paddleY = SCREEN_HEIGHT - 20;
	Ball myBall = createBall( XPOS_START, YPOS_START, XVEL_START, YVEL_START, RADIUS );
	while(!GAMEOVER){
		drawBox(myBall.pos.x, myBall.pos.y, COLOR);
		drawPaddle(paddleX, paddleY, COLOR);
		Delay40ms();
		drawBox(myBall.pos.x, myBall.pos.y, 0);
		drawPaddle(paddleX, paddleY, 0);
		if(!LEFT_BUTTON){
			paddleX -= 5;
			if(paddleX <= 0){paddleX = 0;}
		}
		if(!RIGHT_BUTTON){
			paddleX += 5;
			if(paddleX >= SCREEN_WIDTH - PADDLE_LENGTH){paddleX = SCREEN_WIDTH - PADDLE_LENGTH;}
		}
		myBall = moveBall(myBall);
		if(paddleCollision(myBall, paddleX, paddleY)){
			myBall.vel.y *= -1;
			score += 1;
		}
		if(bottomCollision(myBall)){
			GAMEOVER = TRUE;
		}
	}
}

void main() {
	// === Initialize system ================================================
	IFG1=0; /* clear interrupt flag1 */
	WDTCTL= WDTPW+WDTHOLD; /* stop WD */

	initMSP();
	Delay160ms();
	initLCD();
	Delay160ms();
	clearScreen();

//	requiredFunctionality();
//	BFunctionality();
	AFunctionality();
}
